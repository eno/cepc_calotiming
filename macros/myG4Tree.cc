#include "myG4Tree.hh"
#include <vector>

void InitG4Tree (TTree * TreeRun, myG4TreeVars& treeVars)
{

    
    TreeRun->SetBranchAddress("Event", &treeVars.Event);    
    
    TreeRun->SetBranchAddress("inputTrackerX0",         &treeVars.inputTrackerX0);    
    TreeRun->SetBranchAddress("inputServiceAlmm",       &treeVars.inputServiceAlmm);    
    TreeRun->SetBranchAddress("inputTimingThick",       &treeVars.inputTimingThick);    
    TreeRun->SetBranchAddress("inputE1Thick",           &treeVars.inputE1Thick);    
    TreeRun->SetBranchAddress("inputE2Thick",           &treeVars.inputE2Thick);    
    TreeRun->SetBranchAddress("inputE1Width",           &treeVars.inputE1Width);    
    TreeRun->SetBranchAddress("inputTimingECAL_dist",   &treeVars.inputTimingECAL_dist);    
    
    /*
    treeVars.inputInitialPosition	= new std::vector<float>(3,0.);
    treeVars.inputMomentum		= new std::vector<float>(4,0.);
    treeVars.primaryPosT1		= new std::vector<float>(3,0.); 
    treeVars.primaryMomT1		= new std::vector<float>(4,0.); 
    treeVars.primaryPosE1		= new std::vector<float>(3,0.); 
    treeVars.primaryMomE1		= new std::vector<float>(4,0.);  
    */
    
    TreeRun->SetBranchAddress("inputMomentum",          &treeVars.inputMomentum);
    TreeRun->SetBranchAddress("inputInitialPosition",   &treeVars.inputInitialPosition);    
    TreeRun->SetBranchAddress("primaryPosT1",           &treeVars.primaryPosT1);
    TreeRun->SetBranchAddress("primaryMomT1",           &treeVars.primaryMomT1);    
    TreeRun->SetBranchAddress("primaryPosE1",           &treeVars.primaryPosE1);
    TreeRun->SetBranchAddress("primaryMomE1",           &treeVars.primaryMomE1);
    
    TreeRun->SetBranchAddress("nTracksT1",          &treeVars.nTracksT1);        
    TreeRun->SetBranchAddress("nTracksT2",          &treeVars.nTracksT2);    
    TreeRun->SetBranchAddress("nTracksE1",          &treeVars.nTracksE1);    
    TreeRun->SetBranchAddress("nTracksE2",          &treeVars.nTracksE2);    
    TreeRun->SetBranchAddress("nTracksTRK",         &treeVars.nTracksTRK);    
    
    TreeRun->SetBranchAddress("depositedEnergyTotal",   &treeVars.depositedEnergyTotal);       
    TreeRun->SetBranchAddress("depositedEnergyTotTRKVolume",   &treeVars.depositedEnergyTotTRKVolume);       
    TreeRun->SetBranchAddress("depositedEnergyTiming_f",&treeVars.depositedEnergyTiming_f);
    TreeRun->SetBranchAddress("depositedEnergyTiming_r",&treeVars.depositedEnergyTiming_r);    
    TreeRun->SetBranchAddress("depositedEnergyECAL_f",  &treeVars.depositedEnergyECAL_f);
    TreeRun->SetBranchAddress("depositedEnergyECAL_r",  &treeVars.depositedEnergyECAL_r);    
    TreeRun->SetBranchAddress("depositedEnergyHCAL",    &treeVars.depositedEnergyHCAL);    
    TreeRun->SetBranchAddress("depositedEnergyHCAL_ScintOnly",    &treeVars.depositedEnergyHCAL_ScintOnly);  
    TreeRun->SetBranchAddress("depositedEnergyWorld",   &treeVars.depositedEnergyWorld);
        
    TreeRun->SetBranchAddress("tot_phot_cer_Timing_f",  &treeVars.tot_phot_cer_Timing_f);    
    TreeRun->SetBranchAddress("tot_phot_cer_Timing_r",  &treeVars.tot_phot_cer_Timing_r);    
    TreeRun->SetBranchAddress("tot_phot_cer_ECAL_f",    &treeVars.tot_phot_cer_ECAL_f);    
    TreeRun->SetBranchAddress("tot_phot_cer_ECAL_r",    &treeVars.tot_phot_cer_ECAL_r);    
    TreeRun->SetBranchAddress("tot_phot_cer_HCAL",      &treeVars.tot_phot_cer_HCAL);        
        
    TreeRun->SetBranchAddress("Edep_Tracker_layer",   &treeVars.Edep_Tracker_layer);
    TreeRun->SetBranchAddress("Edep_Timing_f_ch",   &treeVars.Edep_Timing_f_ch);
    TreeRun->SetBranchAddress("Edep_Timing_r_ch",   &treeVars.Edep_Timing_r_ch);
    TreeRun->SetBranchAddress("Edep_ECAL_f_ch",     &treeVars.Edep_ECAL_f_ch);
    TreeRun->SetBranchAddress("Edep_ECAL_r_ch",     &treeVars.Edep_ECAL_r_ch);
    
//     treeVars.gamma_brem  = new std::vector<float>;
//     TreeRun->SetBranchAddress("gamma_brem",     &treeVars.gamma_brem);


}
