
#ifndef __myG4Tree__
#define __myG4Tree__

#include <vector>
#include <string>
#include <algorithm>

#include "TTree.h"
#include "TChain.h"
#include "TH1F.h"

struct myG4TreeVars
{

  /// defining tree variables            
    
    
  int Event;

  Float_t inputTrackerX0;
  Float_t inputServiceAlmm;
  Float_t inputTimingThick;
  Float_t inputE1Thick;
  Float_t inputE2Thick;
  Float_t inputE1Width;
  Float_t inputTimingECAL_dist;

  /*
  std::vector<float> *inputMomentum ; // Px Py Pz E
  std::vector<float> *inputInitialPosition ; // x, y, z
  std::vector<float> *primaryMomT1 ; // Px Py Pz E
  std::vector<float> *primaryPosT1 ; // x, y, z
  std::vector<float> *primaryMomE1 ; // Px Py Pz E
  std::vector<float> *primaryPosE1 ; // x, y, z
  */

  float inputMomentum[4] ; // Px Py Pz E
  float inputInitialPosition[3] ; // x, y, z
  float primaryMomT1[4] ; // Px Py Pz E
  float primaryPosT1[3] ; // x, y, z
  float primaryMomE1[4] ; // Px Py Pz E
  float primaryPosE1[3] ; // x, y, z
  
  int nTracksT1;
  int nTracksT2;
  int nTracksE1;
  int nTracksE2;
  int nTracksTRK[6];

  //integrated energy in each longitudinal layer
  float depositedEnergyTotal;
  float depositedEnergyTotTRKVolume;
  float depositedEnergyTiming_f;
  float depositedEnergyTiming_r;
  float depositedEnergyECAL_f;
  float depositedEnergyECAL_r;
  float depositedEnergyHCAL;
  float depositedEnergyHCAL_ScintOnly;

  float nonIonDepEnergyTotTRKVolume;
  float nonIonDepEnergyTiming_f;
  float nonIonDepEnergyTiming_r;
  float nonIonDepEnergyECAL_f;
  float nonIonDepEnergyECAL_r;
  float nonIonDepEnergyHCAL;

  
  int tot_phot_cer_Timing_f;
  int tot_phot_cer_Timing_r;
  int tot_phot_cer_ECAL_f ;
  int tot_phot_cer_ECAL_r ;
  int tot_phot_cer_HCAL;


  float depositedEnergyWorld;  

  //energy deposit in each trasnversally segmented channel
  float Edep_Tracker_layer[6];
  float Edep_Timing_f_ch[18];
  float Edep_Timing_r_ch[18];
  float Edep_ECAL_f_ch[400];
  float Edep_ECAL_r_ch[400];
  float Edep_HCAL_layer[15];
  float nonIonEdep_HCAL_layer[15];

  TH1F* h_phot_cer_lambda_Timing_f ;
  TH1F* h_phot_cer_lambda_Timing_r;
  TH1F* h_phot_cer_lambda_ECAL_f ;
  TH1F* h_phot_cer_lambda_ECAL_r;
  TH1F* h_phot_cer_lambda_HCAL;
  
//   std::vector<float> *gamma_brem;
  
  
  
  
  
};

void InitG4Tree(TTree* TreeRun, myG4TreeVars &treeVars);


#endif
