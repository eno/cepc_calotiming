#ifndef CreateTree_H
#define CreateTree_H 1

#include <iostream>
#include <vector>
#include <map>
#include "TString.h"

#include "TH2F.h"
#include "TProfile2D.h"
#include "TH3F.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"

class CreateTree
{
private:
  
  TTree*  ftree ;
  TString fname ;
  
public:
  
  CreateTree (TString name);
  ~CreateTree () ;
  
  TTree*          GetTree() const { return ftree; };
  TString         GetName() const { return fname; };
  void             AddEnergyDeposit(int index, float deposit);
  void             AddScintillationPhoton(int index);
  void             AddCerenkovPhoton(int index);
  int                Fill();
  bool             Write(TFile *);
  void             Clear() ;
  
  static CreateTree* Instance() { return fInstance; } ;
  static CreateTree* fInstance;
  
  int Event;

  float inputTrackerX0;
  float inputServiceAlmm;
  float inputTimingThick;
  float inputE1Thick;
  float inputE2Thick;
  float inputE1Width;
  float inputQuartzSize;
  float inputTimingECAL_dist;

  
  float inputMomentum[4] ; // Px Py Pz E
  float inputInitialPosition[3] ; // x, y, z

  float primaryMomT1[4] ; // Px Py Pz E
  float primaryPosT1[3] ; // x, y, z

  float primaryMomE1[4] ; // Px Py Pz E
  float primaryPosE1[3] ; // x, y, z

  int nTracksT1;
  int nTracksT2;
  int nTracksE1;
  int nTracksE2;
  int nTracksTRK[6];

  //integrated energy in each longitudinal layer

  float depositedEnergyEscapeWorld;


  float depositedEnergyTotal;
  float depositedEnergyTotTRKVolume;
  float depositedIonEnergyTotTRKVolume;
  float depositednonIonEnergyTotTRKVolume;


  float depositedEnergyTiming_f;
  float depositedEnergyTiming_r;

    

float depositedEnergyECAL_f;
  float depositedEnergyECAL_r;





  float depositedEnergyHCALAct;
  float depositedEnergyHCALPas;
  float depositedEnergyServices;
  float depositedEnergyTimingGap;
  float depositedEnergyEcalGap;
  float depositedEnergyEcalDet;
  float depositedEnergySolenoid;
  float depositedEnergyWorld;
  float depositedEnergyQuartz;

  float depositedIonEnergyTotal;
  float depositedIonEnergyTiming_f;
  float depositedIonEnergyTiming_r;
  float depositedIonEnergyECAL_f;
  float depositedIonEnergyECAL_r;
  float depositedIonEnergyHCALAct;
  float depositedIonEnergyHCALPas;
  float depositedIonEnergyServices;
  float depositedIonEnergyTimingGap;
  float depositedIonEnergyEcalGap;
  float depositedIonEnergyEcalDet;
  float depositedIonEnergySolenoid;
  float depositedIonEnergyWorld;
  float depositedIonEnergyQuartz;


  float depositednonIonEnergyTotal;
  float depositednonIonEnergyTiming_f;
  float depositednonIonEnergyTiming_r;
  float depositednonIonEnergyECAL_f;
  float depositednonIonEnergyECAL_r;
  float depositednonIonEnergyHCALAct;
  float depositednonIonEnergyHCALPas;
  float depositednonIonEnergyServices;
  float depositednonIonEnergyTimingGap;
  float depositednonIonEnergyEcalGap;
  float depositednonIonEnergyEcalDet;
  float depositednonIonEnergySolenoid;
  float depositednonIonEnergyWorld;
  float depositednonIonEnergyQuartz;


  float depositedElecEnergyTotal;
  float depositedElecEnergyTiming_f;
  float depositedElecEnergyTiming_r;
  float depositedElecEnergyECAL_f;
  float depositedElecEnergyECAL_r;
  float depositedElecEnergyHCALAct;
  float depositedElecEnergyHCALPas;
  float depositedElecEnergyServices;
  float depositedElecEnergyTimingGap;
  float depositedElecEnergyEcalGap;
  float depositedElecEnergyEcalDet;
  float depositedElecEnergySolenoid;
  float depositedElecEnergyWorld;
  float depositedElecEnergyQuartz;
  

  int tot_phot_cer_Timing_f;
  int tot_phot_cer_Timing_r;
  int tot_phot_cer_ECAL_f ;
  int tot_phot_cer_ECAL_r ;
  int tot_phot_cer_Quartz ;
  int tot_phot_cer_HCAL;



  float Edep_Tracker_layer[6];

  //energy deposit in each trasnversally segmented channel
  float Edep_Timing_f_ch[18];
  float Edep_Timing_r_ch[18];

  float Edep_ECAL_f_ch[400];
  float Edep_ECAL_r_ch[400];
  float Edep_HCAL_layer[50];
  float nonIonEdep_HCAL_layer[50];


  // std::vector<float> *gamma_brem; 
  // std::vector<float> *ele_ioni; 

//  std::vector<float> E_dep_crystal;



  TH1F* h_phot_cer_lambda_Timing_f ;
  TH1F* h_phot_cer_lambda_Timing_r;
  TH1F* h_phot_cer_lambda_ECAL_f ;
  TH1F* h_phot_cer_lambda_ECAL_r;
  TH1F* h_phot_cer_lambda_Quartz;
  TH1F* h_phot_cer_lambda_HCAL;
  TH1F* h_phot_cer_parentID;


};

#endif
